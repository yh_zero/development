# 前端面试集合-都是小浩经历过的面试题

##  **1.[什么是闭包！](https://my.oschina.net/u/4208537/blog/5083355)**   
在本质上 ，闭包就是将函数内部和函数外部连接起来的的一座桥梁！有两个好处：一个是可以读取函数内部的变量，另外一个是让这些变量的值始终保持在内存中。
## 2.[斐波那契数列](https://my.oschina.net/u/4208537/blog/5083526)

普通递归
```
function f(n){
    if(n<=2){
        return 1
    }
    return f(n-1) +f(n-2)
}

```
## 3.[冒泡排序](https://my.oschina.net/yangxiaohao/blog/5084488)

```
// 冒泡排序 从小到大
let arr = [ 23, 5, 100, 8888, 600, 700, 200 ], t;
for(let i=0; i<arr.length-1; i++){//一共多少轮
	for(let j=0; j<arr.length-1-i; j++){//一轮多少次
		if(arr[j] > arr[j+1]){//想要大到小 改成 <
			t = arr[j]
			arr[j] = arr[j+1]
			arr[j+1] = t
		}
	}
}
console.log(arr)
```

## 4.[防抖_节流](https://my.oschina.net/yangxiaohao/blog/5085797)
###  防抖

```
<input type="button" value="按钮" />
<script type="text/javascript">
	const button = document.querySelector('input')
	function payMoney() {//发送数据
		console.log("发送数据成功")
	}
	// 防止小明 防抖函数 
	function debounce(func, delay) {
		// func()	// 不可以这样写 会直接运行
		let timer;//程序执行多少时间
		return function() {
			let context = this;//保存this
			let args = arguments;
			clearTimeout(timer);//清除延迟
			timer = setTimeout(function () {//执行了多少时间
				func.apply(context, args);
			},delay)
		}
	}
	button.addEventListener("click", debounce(payMoney,1000))
</script>
```

## 5.[宏任务 _ 微任务 -JavaScript](https://my.oschina.net/yangxiaohao/blog/5088338)


```
console.log("1")
setTimeout(()=>{
	console.log("2")
})
new Promise((resolve)=> {
	resolve()
	console.log("3")
	
}).then(()=>{
	console.log("4")
}).finally(()=>{
	console.log("5")
})
console.log("6")
```
### 结果：1 3 6 4 5 2

## 6.[前端面试题 _ 浅拷贝 _ 深拷贝]( https://my.oschina.net/yangxiaohao/blog/5086250)
### 深拷贝

```
const oldObj = {
	name: "杨小浩",
	age: 18,
	hobby:['打代码','看书', '运动'],
	friends:{
		name: '小明',
		age: '20',
	},
}
// 深拷贝
function deepClone(obj = {}){
	if(!obj || typeof obj !== 'object'){
		return obj
	}
	let result = Array.isArray(obj) ? [] : {}
	for(let key in obj){
		if(obj.hasOwnProperty(key)){
			result[key] = deepClone(obj[key])//递归
		}
	}
	return result
}
const newObj3 = deepClone(oldObj);
newObj3.friends.name = '小明123'
newObj3.hobby[0] = "看美女"
newObj3.age = 16
console.log("newObj3", newObj3)
console.log("oldObj", oldObj)
```
## 7.[JavaScript _ 创建对象](https://my.oschina.net/yangxiaohao/blog/5118907)

```
//工厂模式
function initPerson(name, age, family, hobby){
	var o = new Object();
	o.name = name
	o.age = age
	o.family = family
	o.hobby = function() {
		console.log(hobby)
	}
	return o
}
let person3 = initPerson("杨小浩", 18, ['111', '222', '333'], "打篮球")
```

